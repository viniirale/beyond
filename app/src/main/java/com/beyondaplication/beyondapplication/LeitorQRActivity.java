package com.beyondaplication.beyondapplication;


import android.view.View;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.journeyapps.barcodescanner.ViewfinderView;

import static android.R.attr.prompt;
import static com.beyondaplication.beyondapplication.R.drawable.camera;


public class LeitorQRActivity extends AppCompatActivity {
    private Button BtnBUIC;
    //private TextView scanResults;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zxing_barcode_scanner);
       // scanResults = (TextView) findViewById(R.id.resultado);
        BtnBUIC = (Button) findViewById(R.id.botaobuic);


        final Activity activity = this;

        IntentIntegrator integrator = new IntentIntegrator(activity);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Com o leitor de QR-CODE do Beyond App, scaneie o código BUIC localizado na etiqueta do produto Beyond Socket");
        integrator.setCameraId(0);  //camera 0 - camera normal, camera 1 - camera frontal
        integrator.setBeepEnabled(false);   //emite um beep ao scanear o QR-code
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();

        BtnBUIC.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                finish();
            }
        });




    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null){
            if(result.getContents()==null){
                Toast.makeText(this, "você cancelou o scaneamento", Toast.LENGTH_LONG).show();
                finish();
            }
            else {
                //Toast.makeText(this, result.getContents(),Toast.LENGTH_LONG).show();    //aqui mostra o resultado do QRcode
                //scanResults.setText(result.getContents());
                finish();
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
