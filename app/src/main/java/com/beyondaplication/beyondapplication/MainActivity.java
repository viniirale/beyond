package com.beyondaplication.beyondapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by Vinicius on 13/03/2017.
 */

public class MainActivity extends AppCompatActivity {
    private Button botaoproduto;
    private Button botaoAC;
    private Button botaoTV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botaoproduto = (Button) findViewById(R.id.botaoproduto);
        botaoAC = (Button) findViewById(R.id.botaoAC);
        botaoTV = (Button) findViewById(R.id.botaoTV);


        botaoproduto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,InstrucaoActivity.class));
            }
        });

    }
}
