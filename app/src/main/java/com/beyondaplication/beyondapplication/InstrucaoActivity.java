package com.beyondaplication.beyondapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by Vinicius on 13/03/2017.
 */

public class InstrucaoActivity extends AppCompatActivity {
    private Button Btnproximo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instrucao);


        Btnproximo = (Button) findViewById(R.id.button1);

        Btnproximo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startActivity(new Intent(InstrucaoActivity.this,LeitorQRActivity.class));
            }
        });
    }
}
